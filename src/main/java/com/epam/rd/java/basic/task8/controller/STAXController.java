package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.bean.Flowers;
import org.w3c.dom.Attr;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.util.StreamReaderDelegate;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Flow;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;
    private String xsdFileName;
    private String xmlns;
    private String xmlns_xsi;
    private String xsi_schemaLocation;
    private Flowers container;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        try {
            XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
            XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(this.getXmlFileName()));

            container = null;
            Flowers.Flower item = null;
            Flowers.Flower.VisualParameters vp = null;
            Flowers.Flower.VisualParameters.AveLenFlower vpa = null;
            Flowers.Flower.GrowingTips gt = null;
            Flowers.Flower.GrowingTips.Watering gtw = null;
            Flowers.Flower.GrowingTips.Tempreture gtt = null;
            Flowers.Flower.GrowingTips.Lighting gtl = null;
            while (reader.hasNext()) {
                XMLEvent nextEvent = reader.nextEvent();
                if (nextEvent.isStartElement()) {
                    StartElement startElement = nextEvent.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case "flowers":
                            xsi_schemaLocation = startElement.getAttributes().next().getValue();
                            List<Namespace> nss = new ArrayList<>();
                            startElement.getNamespaces().forEachRemaining(nss::add);
                            xmlns = nss.get(0).getValue();
                            xmlns_xsi = nss.get(1).getValue();
                            xsdFileName = xsi_schemaLocation.split(" ")[1];
                            container = new Flowers();
                            break;
                        case "flower":
                            item = new Flowers.Flower();
                            break;
                        case "name":
                            nextEvent = reader.nextEvent();
                            item.setName(nextEvent.asCharacters().getData());
                            break;
                        case "soil":
                            nextEvent = reader.nextEvent();
                            item.setSoil(nextEvent.asCharacters().getData());
                            break;
                        case "origin":
                            nextEvent = reader.nextEvent();
                            item.setOrigin(nextEvent.asCharacters().getData());
                            break;
                        case "visualParameters":
                            vp = new Flowers.Flower.VisualParameters();
                            break;
                        case "stemColour":
                            nextEvent = reader.nextEvent();
                            vp.setStemColour(nextEvent.asCharacters().getData());
                            break;
                        case "leafColour":
                            nextEvent = reader.nextEvent();
                            vp.setLeafColour(nextEvent.asCharacters().getData());
                            break;
                        case "aveLenFlower":
                            vpa = new Flowers.Flower.VisualParameters.AveLenFlower();
                            vpa.setMeasure(nextEvent.asStartElement().getAttributeByName(new QName("measure")).getValue());
                            nextEvent = reader.nextEvent();
                            vpa.setValue(nextEvent.asCharacters().getData());
                            vp.setAveLenFlower(vpa);
                            item.setVisualParameters(vp);
                            break;
                        case "growingTips":
                            gt = new Flowers.Flower.GrowingTips();
                            break;
                        case "tempreture":
                            gtt = new Flowers.Flower.GrowingTips.Tempreture();
                            gtt.setMeasure(nextEvent.asStartElement().getAttributeByName(new QName("measure")).getValue());
                            nextEvent = reader.nextEvent();
                            gtt.setValue(nextEvent.asCharacters().getData());
                            gt.setTempreture(gtt);
                            break;
                        case "lighting":
                            gtl = new Flowers.Flower.GrowingTips.Lighting();
                            gtl.setLightRequiring(nextEvent.asStartElement().getAttributeByName(new QName("lightRequiring")).getValue());
                            gt.setLighting(gtl);
                            break;
                        case "watering":
                            gtw = new Flowers.Flower.GrowingTips.Watering();
                            gtw.setMeasure(nextEvent.asStartElement().getAttributeByName(new QName("measure")).getValue());
                            nextEvent = reader.nextEvent();
                            gtw.setValue(nextEvent.asCharacters().getData());
                            gt.setWatering(gtw);
                            item.setGrowingTips(gt);
                            break;
                        case "multiplying":
                            nextEvent = reader.nextEvent();
                            item.setMultiplying(nextEvent.asCharacters().getData());
                            container.getFlower().add(item);
                            break;
                        default:
                            break;
                    }
                }
            }
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        } catch (NullPointerException ne) {
            System.err.println("NULL ERROR: " + ne.getMessage());
            throw ne;
        }
    }

    public void writeXml(OutputStream out) throws XMLStreamException {

        XMLOutputFactory output = XMLOutputFactory.newInstance();

        XMLStreamWriter writer = output.createXMLStreamWriter(out);

        writer.writeStartDocument("utf-8", "1.0");

        // <company>
        writer.writeStartElement("flowers");
        writer.writeAttribute("xmlns",this.xmlns);
        writer.writeAttribute("xmlns:xsi",this.xmlns_xsi);
        writer.writeAttribute("xsi:schemaLocation",this.xsi_schemaLocation);

        // <staff>
        getContainer().getFlower().forEach(x->{
            try {
                writer.writeStartElement("flower");

                writer.writeStartElement("name");
                writer.writeCharacters(x.getName());
                writer.writeEndElement();

                writer.writeStartElement("soil");
                writer.writeCharacters(x.getSoil());
                writer.writeEndElement();

                writer.writeStartElement("origin");
                writer.writeCharacters(x.getOrigin());
                writer.writeEndElement();

                writer.writeStartElement("visualParameters");

                writer.writeStartElement("stemColour");
                writer.writeCharacters(x.getVisualParameters().getStemColour());
                writer.writeEndElement();

                writer.writeStartElement("leafColour");
                writer.writeCharacters(x.getVisualParameters().getLeafColour());
                writer.writeEndElement();

                writer.writeStartElement("aveLenFlower");
                writer.writeAttribute("measure",x.getVisualParameters().getAveLenFlower().getMeasure());
                writer.writeCharacters(x.getVisualParameters().getAveLenFlower().getValue());
                writer.writeEndElement();

                writer.writeEndElement();

                writer.writeStartElement("growingTips");

                writer.writeStartElement("tempreture");
                writer.writeAttribute("measure",x.getGrowingTips().getTempreture().getMeasure());
                writer.writeCharacters(x.getGrowingTips().getTempreture().getValue());
                writer.writeEndElement();

                writer.writeEmptyElement("lighting");
                writer.writeAttribute("lightRequiring",x.getGrowingTips().getLighting().getLightRequiring());

                writer.writeStartElement("watering");
                writer.writeAttribute("measure",x.getGrowingTips().getWatering().getMeasure());
                writer.writeCharacters(x.getGrowingTips().getWatering().getValue());
                writer.writeEndElement();

                writer.writeEndElement();

                writer.writeStartElement("multiplying");
                writer.writeCharacters(x.getMultiplying());
                writer.writeEndElement();
                writer.writeEndElement();

            } catch (XMLStreamException e) {
                e.printStackTrace();
            }
        });

        writer.writeEndDocument();
        writer.flush();
        writer.close();

    }

    public Flowers getContainer() {
        return container;
    }

    public String getXmlFileName() {
        return xmlFileName;
    }
}