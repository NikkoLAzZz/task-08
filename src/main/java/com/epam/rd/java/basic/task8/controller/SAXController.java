package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.bean.Flowers;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.text.ParseException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private String xmlns;
    private String xmlns_xsi;
    private String xsi_schemaLocation;

    private StringBuilder currentValue = new StringBuilder();
    private Flowers.Flower currentFlower;
    private Flowers flowerContainer;

    private Flowers.Flower.VisualParameters vp;
    private Flowers.Flower.VisualParameters.AveLenFlower vpa;

    private Flowers.Flower.GrowingTips gt;
    private Flowers.Flower.GrowingTips.Lighting gtl;
    private Flowers.Flower.GrowingTips.Tempreture gtt;
    private Flowers.Flower.GrowingTips.Watering gtw;

    private final String xmlPath;

    public SAXController(String xmlPath, String xsdPath) {
        this.xmlPath = xmlPath;
        try {
            validateWithExtXSDUsingSAX(xmlPath, xsdPath);
            SAXParserFactory sxp = SAXParserFactory.newInstance();
            SAXParser saxParser = sxp.newSAXParser();
            saxParser.parse(getXmlPath(), this);
            this.toXml(xmlPath);
        } catch (SAXException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }
    }

    public String getXmlPath() {
        return xmlPath;
    }

    public Flowers getFlowerContainer() {
        return flowerContainer;
    }

    public static boolean validateWithExtXSDUsingSAX(String xml, String xsd) throws ParserConfigurationException, IOException, SAXException {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setValidating(false);
            factory.setNamespaceAware(true);

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            SAXParser parser;
            try {
                factory.setSchema(schemaFactory.newSchema(new Source[]{new StreamSource(xsd)}));
                parser = factory.newSAXParser();
            } catch (SAXException se) {
                System.out.println("SCHEMA : " + se.getMessage());  // problem in the XSD itself
                return false;
            }

            XMLReader reader = parser.getXMLReader();
            reader.setErrorHandler(new ErrorHandler() {
                public void warning(SAXParseException e) throws SAXException {
                    System.out.println("WARNING: " + e.getMessage()); // do nothing
                }

                public void error(SAXParseException e) throws SAXException {
                    System.out.println("ERROR : " + e.getMessage());
                    throw e;
                }

                public void fatalError(SAXParseException e) throws SAXException {
                    System.out.println("FATAL : " + e.getMessage());
                    throw e;
                }
            });
            reader.parse(new InputSource(xml));
            return true;
        } catch (ParserConfigurationException pce) {
            throw pce;
        } catch (IOException io) {
            throw io;
        } catch (SAXException se) {
            throw se;
        }
    }


    public void toXml(String outputXML) {
        try {
            OutputStream outputStream = new FileOutputStream(new File(outputXML));

            XMLStreamWriter out = XMLOutputFactory.newInstance().createXMLStreamWriter(new OutputStreamWriter(outputStream, "utf-8"));

            out.writeStartDocument();
            out.writeStartElement("flowers");
            out.writeAttribute("xmlns", xmlns);
            out.writeAttribute("xmlns:xsi", xmlns_xsi);
            out.writeAttribute("xsi:schemaLocation", xsi_schemaLocation);

            flowerContainer.getFlower().forEach(x -> {
                try {
                    out.writeStartElement("flower");

                    out.writeStartElement("name");
                    out.writeCharacters(x.getName());
                    out.writeEndElement();

                    out.writeStartElement("soil");
                    out.writeCharacters(x.getSoil());
                    out.writeEndElement();

                    out.writeStartElement("origin");
                    out.writeCharacters(x.getOrigin());
                    out.writeEndElement();

                    out.writeStartElement("visualParameters");
                    out.writeStartElement("stemColour");
                    out.writeCharacters(x.getVisualParameters().getStemColour());
                    out.writeEndElement();
                    out.writeStartElement("leafColour");
                    out.writeCharacters(x.getVisualParameters().getLeafColour());
                    out.writeEndElement();
                    out.writeStartElement("aveLenFlower");
                    out.writeAttribute("measure", x.getVisualParameters().getAveLenFlower().getMeasure());
                    out.writeCharacters(x.getVisualParameters().getAveLenFlower().getValue());
                    out.writeEndElement();
                    out.writeEndElement();

                    out.writeStartElement("growingTips");
                    out.writeStartElement("tempreture");
                    out.writeAttribute("measure", x.getGrowingTips().getTempreture().getMeasure());
                    out.writeCharacters(x.getGrowingTips().getTempreture().getValue());
                    out.writeEndElement();
                    out.writeEmptyElement("lighting");
                    out.writeAttribute("lightRequiring", x.getGrowingTips().getLighting().getLightRequiring());
                    out.writeStartElement("watering");
                    out.writeAttribute("measure", x.getGrowingTips().getWatering().getMeasure());
                    out.writeCharacters(x.getGrowingTips().getWatering().getValue());
                    out.writeEndElement();
                    out.writeEndElement();

                    out.writeStartElement("multiplying");
                    out.writeCharacters(x.getMultiplying());
                    out.writeEndElement();

                    out.writeEndElement();
                } catch (XMLStreamException e) {
                    e.printStackTrace();
                }
            });
            out.writeEndDocument();

            out.close();
        } catch (XMLStreamException | FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentValue.setLength(0);
        switch (qName) {
            case "flowers":
                xmlns = attributes.getValue(0);
                xmlns_xsi = attributes.getValue(1);
                xsi_schemaLocation = attributes.getValue(2);
            case "flower":
                currentFlower = new Flowers.Flower();
                break;
            case "visualParameters":
                vp = new Flowers.Flower.VisualParameters();
                break;
            case "aveLenFlower":
                vpa = new Flowers.Flower.VisualParameters.AveLenFlower();
                vpa.setMeasure(attributes.getValue(0));
                break;
            case "growingTips":
                gt = new Flowers.Flower.GrowingTips();
                break;
            case "tempreture":
                gtt = new Flowers.Flower.GrowingTips.Tempreture();
                gtt.setMeasure(attributes.getValue(0));
                break;
            case "lighting":
                gtl = new Flowers.Flower.GrowingTips.Lighting();
                gtl.setLightRequiring(attributes.getValue(0));
                gt.setLighting(gtl);
                break;
            case "watering":
                gtw = new Flowers.Flower.GrowingTips.Watering();
                gtw.setMeasure(attributes.getValue(0));
                break;
            default:
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        currentValue.append(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case "name":
                currentFlower.setName(currentValue.toString());
                break;
            case "soil":
                currentFlower.setSoil(currentValue.toString());
                break;
            case "origin":
                currentFlower.setOrigin(currentValue.toString());
                break;
            case "stemColour":
                vp.setStemColour(currentValue.toString());
                break;
            case "leafColour":
                vp.setLeafColour(currentValue.toString());
                break;
            case "aveLenFlower":
                vpa.setValue(currentValue.toString());
                vp.setAveLenFlower(vpa);
                break;
            case "visualParameters":
                currentFlower.setVisualParameters(vp);
                break;
            case "tempreture":
                gtt.setValue(currentValue.toString());
                gt.setTempreture(gtt);
                break;
            case "watering":
                gtw.setValue(currentValue.toString());
                gt.setWatering(gtw);
                break;
            case "growingTips":
                currentFlower.setGrowingTips(gt);
                break;
            case "multiplying":
                currentFlower.setMultiplying(currentValue.toString());
                break;
            case "flower":
                flowerContainer.getFlower().add(currentFlower);
                currentFlower = null;
                break;
            default:
                break;
        }
    }

    @Override
    public void startDocument() throws SAXException {
        flowerContainer = new Flowers();

    }

    @Override
    public void endDocument() throws SAXException {
        super.endDocument();

    }
}