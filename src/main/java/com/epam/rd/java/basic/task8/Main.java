package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.bean.Flowers;
import com.epam.rd.java.basic.task8.controller.*;

import java.io.FileOutputStream;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		DOMController domController = new DOMController(xmlFileName);
		Flowers flowersDOM = domController.xmlToFlowers();
		String outputXmlFile = "output.dom.xml";
		domController.innerFlowersToXML(outputXmlFile);
		System.out.println("Output ==> " + outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		SAXController saxController = new SAXController(xmlFileName, domController.getXsdFileName());
		Flowers flowersSAX = saxController.getFlowerContainer();
		outputXmlFile = "output.sax.xml";
		saxController.toXml(outputXmlFile);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		STAXController staxController = new STAXController(xmlFileName);
		outputXmlFile = "output.stax.xml";
		staxController.writeXml(new FileOutputStream(outputXmlFile));
	}

}
