package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.bean.Flowers;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;

/**
 * Controller for DOM parser.
 */
public class DOMController {


	private String xmlns;
	private String xmlns_xsi;
	private String xsi_schemaLocation;

	private String xsdFileName;
	private String xmlFileName;
 	private Flowers flowers;
	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void setFlowers(Flowers flowers) {
		this.flowers = flowers;
	}

	public Flowers xmlToFlowers(){
		this.flowers = new Flowers();
		try {
			DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
			DocumentBuilder db1 = dbf1.newDocumentBuilder();
			Document dom1 = db1.parse(new InputSource(new FileInputStream(xmlFileName)));
			NamedNodeMap attributes = dom1.getElementsByTagName("flowers").item(0).getAttributes();

			xmlns = attributes.item(0).getTextContent();
			xmlns_xsi = attributes.item(1).getTextContent();
			xsi_schemaLocation = attributes.item(2).getTextContent();
			xsdFileName = xsi_schemaLocation.split(" ")[1];

			InputStream xml = new FileInputStream(xmlFileName);
			InputStream xsd = new FileInputStream(xsdFileName);

			//Validate
			SchemaFactory xsf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema xs = xsf.newSchema(new StreamSource(xsd));

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			//!!!!!!
			dbf.setValidating(false);
			dbf.setSchema(xs);

			DocumentBuilder db = dbf.newDocumentBuilder();
			Document dom = db.parse(new InputSource(xml));

			NodeList list = dom.getElementsByTagName("flower");
			for (int i = 0; i < list.getLength(); i++) {
				Node node = list.item(i);
				Element element = (Element) node;

				String name = element.getElementsByTagName("name").item(0).getTextContent();
				String soil = element.getElementsByTagName("soil").item(0).getTextContent();
				String origin = element.getElementsByTagName("origin").item(0).getTextContent();

				Element visualParameters = (Element) element.getElementsByTagName("visualParameters").item(0);
				String stemColour = visualParameters.getElementsByTagName("stemColour").item(0).getTextContent();
				String leafColour = visualParameters.getElementsByTagName("leafColour").item(0).getTextContent();
				String aveLenFlower = visualParameters.getElementsByTagName("aveLenFlower").item(0).getTextContent();
				String measureVP = visualParameters.getElementsByTagName("aveLenFlower").item(0).getAttributes().item(0).getTextContent();

				Element growingTips = (Element) element.getElementsByTagName("growingTips").item(0);
				String tempreture = growingTips.getElementsByTagName("tempreture").item(0).getTextContent();
				String measureT = growingTips.getElementsByTagName("tempreture").item(0).getAttributes().item(0).getTextContent();

				String lightRequiring = growingTips.getElementsByTagName("lighting").item(0).getAttributes().item(0).getTextContent();

				String watering = growingTips.getElementsByTagName("watering").item(0).getTextContent();
				String measureW = growingTips.getElementsByTagName("watering").item(0).getAttributes().item(0).getTextContent();

				String multiplying = element.getElementsByTagName("multiplying").item(0).getTextContent();


				Flowers.Flower flower;
				{
					flower = new Flowers.Flower();
					flower.setName(name);
					flower.setSoil(soil);
					flower.setOrigin(origin);

					Flowers.Flower.VisualParameters vp = new Flowers.Flower.VisualParameters();
					vp.setStemColour(stemColour);
					vp.setLeafColour(leafColour);
					Flowers.Flower.VisualParameters.AveLenFlower aveLenFlower1 = new Flowers.Flower.VisualParameters.AveLenFlower();
					aveLenFlower1.setMeasure(measureVP);
					aveLenFlower1.setValue(aveLenFlower);
					vp.setAveLenFlower(aveLenFlower1);
					flower.setVisualParameters(vp);

					Flowers.Flower.GrowingTips gt = new Flowers.Flower.GrowingTips();
					Flowers.Flower.GrowingTips.Tempreture gtt = new Flowers.Flower.GrowingTips.Tempreture();
					gtt.setMeasure(measureT);
					gtt.setValue(tempreture);
					Flowers.Flower.GrowingTips.Lighting gtl = new Flowers.Flower.GrowingTips.Lighting();
					gtl.setLightRequiring(lightRequiring);
					gt.setLighting(gtl);
					gt.setTempreture(gtt);

					flower.setGrowingTips(gt);

					Flowers.Flower.GrowingTips.Watering gtw = new Flowers.Flower.GrowingTips.Watering();
					gtw.setMeasure(measureW);
					gtw.setValue(watering);

					gt.setWatering(gtw);

					flower.setMultiplying(multiplying);
				}

//				System.out.println(flower);
				this.flowers.getFlower().add(flower);
			}
			xml.close();
			xsd.close();
			dom.getDocumentElement().normalize();
			return this.flowers;
		}catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getXsdFileName() {
		return xsdFileName;
	}

	public void innerFlowersToXML(String output){
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("flowers");
			rootElement.setAttribute("xmlns",xmlns);
			rootElement.setAttribute("xmlns:xsi",xmlns_xsi); //-------------------------------------------------
			rootElement.setAttribute("xsi:schemaLocation",xsi_schemaLocation);
			doc.appendChild(rootElement);

			this.flowers.getFlower().forEach( x-> {

				Element flower = doc.createElement("flower");
				rootElement.appendChild(flower);

				Element name = doc.createElement("name");
				name.setTextContent(x.getName());
				flower.appendChild(name);

				Element role = doc.createElement("soil");
				role.setTextContent(x.getSoil());
				flower.appendChild(role);

				Element origin = doc.createElement("origin");
				origin.setTextContent(x.getOrigin());
				flower.appendChild(origin);

				Element visualParameters = doc.createElement("visualParameters");

				Element stemColour = doc.createElement("stemColour");
				stemColour.setTextContent(x.getVisualParameters().getStemColour());

				Element leafColour = doc.createElement("leafColour");
				leafColour.setTextContent(x.getVisualParameters().getLeafColour());

				Element aveLenFlower = doc.createElement("aveLenFlower");
				aveLenFlower.setAttribute("measure", x.getVisualParameters().getAveLenFlower().getMeasure());
				aveLenFlower.setTextContent(x.getVisualParameters().getAveLenFlower().getValue().toString());

				visualParameters.appendChild(stemColour);
				visualParameters.appendChild(leafColour);
				visualParameters.appendChild(aveLenFlower);

				flower.appendChild(visualParameters);

				Element growingTips = doc.createElement("growingTips");

				Element tempreture = doc.createElement("tempreture");
				tempreture.setAttribute("measure", x.getGrowingTips().getTempreture().getMeasure());
				tempreture.setTextContent(x.getGrowingTips().getTempreture().getValue().toString());
				Element lighting = doc.createElement("lighting");
				lighting.setAttribute("lightRequiring",x.getGrowingTips().getLighting().getLightRequiring());
				Element watering = doc.createElement("watering");
				watering.setAttribute("measure", x.getGrowingTips().getWatering().getMeasure());
				watering.setTextContent(x.getGrowingTips().getWatering().getValue().toString());

				growingTips.appendChild(tempreture);
				growingTips.appendChild(lighting);
				growingTips.appendChild(watering);

				flower.appendChild(growingTips);

				Element multiplying = doc.createElement("multiplying");
				multiplying.setTextContent(x.getMultiplying());

				flower.appendChild(multiplying);
			});

			// print XML to system console
			writeXml(doc, new FileOutputStream(new File(output)));
		} catch (ParserConfigurationException | FileNotFoundException | TransformerException  e) {
			e.printStackTrace();
		}
	}



	private static void writeXml(Document doc,
								 OutputStream output)
			throws TransformerException {

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		// pretty print
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(output);

		transformer.transform(source, result);

	}
}
